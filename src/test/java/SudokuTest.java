import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class SudokuTest {

    private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private final PrintStream printStreamOutput = System.out;
    private final int[] samplePuzzle = new int[]{
            0, 0, 2, 5, 0, 0, 0, 0, 3,
            0, 4, 0, 0, 6, 7, 0, 0, 0,
            1, 5, 0, 0, 0, 3, 0, 0, 0,
            0, 0, 8, 0, 0, 0, 0, 0, 4,
            5, 6, 0, 0, 0, 0, 0, 1, 7,
            4, 0, 0, 0, 0, 0, 8, 0, 0,
            0, 0, 0, 6, 0, 0, 0, 8, 1,
            0, 0, 0, 1, 8, 0, 0, 2, 0,
            2, 0, 0, 0, 0, 5, 7, 0, 0};
    private Sudoku sudoku;

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(byteArrayOutputStream));
        sudoku = new Sudoku(samplePuzzle);
    }

    @AfterEach
    public void restoreStream() {
        System.setOut(printStreamOutput);
    }

    @Test
    void shouldPrintSudoku() {

        sudoku.printSudoku();

        String expectedResult =
                        "  2|5  |  3\n" +
                        " 4 | 67|   \n" +
                        "15 |  3|   \n" +
                        "---+---+---\n" +
                        "  8|   |  4\n" +
                        "56 |   | 17\n" +
                        "4  |   |8  \n" +
                        "---+---+---\n" +
                        "   |6  | 81\n" +
                        "   |18 | 2 \n" +
                        "2  |  5|7  \n";

        assertEquals(expectedResult, byteArrayOutputStream.toString());
    }

    @Test
    void isInsertAllowed() {

        int location = 5;
        int value = 4;

        assertTrue(sudoku.isInsertAllowed(location, value));
    }

    @Test
    void isInsertNotAllowedWhenLocationOfPuzzleIsOutOfMap() {

        int location = 120;
        int value = 2;

        assertFalse(sudoku.isInsertAllowed(location, value));
    }

    @Test
    void isInsertNotAllowedWhenValueOfPuzzleIsWrong() {

        int location = 2;
        int value = 10;

        assertFalse(sudoku.isInsertAllowed(location, value));
    }

    @Test
    void isInsertNotAllowedWhenIsOtherTheSameValueOfPuzzleHorizontally() {

        int location = 23;
        int value = 3;

        assertFalse(sudoku.isInsertAllowed(location, value));
    }

    @Test
    void isInsertNotAllowedWhenIsOtherTheSameValueOfPuzzleVertically() {

        int location = 21;
        int value = 8;

        assertFalse(sudoku.isInsertAllowed(location, value));
    }

    @Test
    void isInsertNotAllowedWhenIsOtherTheSameValueOfPuzzleInSingleBlock() {

        int location = 80;
        int value = 8;

        assertFalse(sudoku.isInsertAllowed(location, value));
    }
}