/*
    @author Szczepan Kras
 */

public class Sudoku {

    private static final int[] samplePuzzle = new int[]{
            0, 0, 2, 5, 0, 0, 0, 0, 3,
            0, 4, 0, 0, 6, 7, 0, 0, 0,
            1, 5, 0, 0, 0, 3, 0, 0, 0,
            0, 0, 8, 0, 0, 0, 0, 0, 4,
            5, 6, 0, 0, 0, 0, 0, 1, 7,
            4, 0, 0, 0, 0, 0, 8, 0, 0,
            0, 0, 0, 6, 0, 0, 0, 8, 1,
            0, 0, 0, 1, 8, 0, 0, 2, 0,
            2, 0, 0, 0, 0, 5, 7, 0, 0};

    private static final int SUDOKU_SIZE = 9;
    private static final int SUDOKU_BLOCK_SIZE = 3;
    private static final int FIRST_SEPARATOR_INDEX = 3;
    private static final int SECOND_SEPARATOR_INDEX = 6;

    private int[] puzzleMap;

    public Sudoku(int[] puzzleMap) {
        this.puzzleMap = puzzleMap;
    }

    public static void main(String[] args) {
        Sudoku sudoku = new Sudoku(samplePuzzle);
        sudoku.printSudoku();

        int location = 5;
        int numberToPlace = 4;

        System.out.printf("For location = %d and number to place = %d insert allowance is %b",
                location, numberToPlace, sudoku.isInsertAllowed(5, 4));
    }

    public void printSudoku() {
        for (int i = 0; i < SUDOKU_SIZE; i++) {
            printBlockVerticalSeparator(i);
            int offset = i * SUDOKU_SIZE;
            System.out.println(formatSudokuLine(offset, offset + SUDOKU_SIZE));
        }
    }

    public boolean isInsertAllowed(int location, int value) {
        return isLocationInTheMap(location)
                && isAllowedValue(value)
                && !isOtherPuzzleInHorizontalLine(location, value)
                && !isOtherPuzzleInVerticalLine(location, value)
                && !isOtherPuzzleInSingleBlock(location, value);
    }

    private static void printBlockVerticalSeparator(int index) {
        if (index == FIRST_SEPARATOR_INDEX || index == SECOND_SEPARATOR_INDEX) {
            System.out.println("---+---+---");
        }
    }

    private String formatSudokuLine(int startIndex, int endIndex) {
        StringBuilder formattedLine = new StringBuilder();
        for (int i = startIndex; i < endIndex; i++) {
            if (isBlockSeparator(i)) {
                formattedLine.append("|");
            }
            formattedLine.append(formatSudokuCell(puzzleMap[i]));
        }
        return formattedLine.toString();
    }

    private boolean isBlockSeparator(int index) {
        return index % SUDOKU_SIZE != 0 && (index % FIRST_SEPARATOR_INDEX == 0 || index % SECOND_SEPARATOR_INDEX == 0);
    }

    private String formatSudokuCell(int cellValue) {
        return cellValue != 0 ? String.valueOf(cellValue) : " ";
    }

    private boolean isLocationInTheMap(int location) {
        return location >= 0 && location < SUDOKU_SIZE * SUDOKU_SIZE;
    }

    private boolean isAllowedValue(int value) {
        return value >= 0 && value <= 9;
    }

    private boolean isOtherPuzzleInHorizontalLine(int location, int value) {
        int line = location / SUDOKU_SIZE;
        int lineLocationStartIndex = line * SUDOKU_SIZE;

        for (int i = lineLocationStartIndex; i < lineLocationStartIndex + SUDOKU_SIZE; i++) {
            if (puzzleMap[i] == value) {
                return true;
            }
        }
        return false;
    }

    private boolean isOtherPuzzleInVerticalLine(int location, int value) {
        int column = (location % SUDOKU_SIZE) - 1;

        for (int i = 0; i < SUDOKU_SIZE * SUDOKU_SIZE; i += 9) {
            if (puzzleMap[i + column] == value) {
                return true;
            }
        }
        return false;
    }

    private boolean isOtherPuzzleInSingleBlock(int location, int value) {
        int line = location / SUDOKU_SIZE;
        int column = location % SUDOKU_SIZE;

        int blockNumberHorizontally = column / SUDOKU_BLOCK_SIZE;
        int blockNumberVertically = line / SUDOKU_BLOCK_SIZE;

        int startIndex = blockNumberHorizontally * SUDOKU_BLOCK_SIZE + blockNumberVertically * SUDOKU_SIZE * SUDOKU_BLOCK_SIZE;

        for (int i = startIndex; i < (startIndex + SUDOKU_BLOCK_SIZE * SUDOKU_SIZE); i += 9) {
            for (int j = i; j < i + SUDOKU_BLOCK_SIZE; j++) {
                if (puzzleMap[j] == value) {
                    return true;
                }
            }
        }
        return false;
    }
}
